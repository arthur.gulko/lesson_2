def parse_cookie(query: str) -> dict:
    query = query.split(";")
    name = "name"
    age = "age"
    result = {}
    for value in query:
        if value.startswith(name):
            new_value = value[5:]
            result[name] = new_value
        if value.startswith(age):
            new_value = value[4:]
            result[age] = new_value
    return result


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}
