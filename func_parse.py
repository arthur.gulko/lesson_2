
import urllib.parse

def parse(query: str) -> dict:
    parsed_url = urllib.parse.urlparse(query)
    query_in_url = parsed_url[4].split("&")
    split_values = [x.split("=") for x in query_in_url]
    result = {}
    for queries in split_values:
        if queries == [""]:
            break
        elif queries != result.keys():
            result[queries[0]] = queries[1]
    return result

if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}